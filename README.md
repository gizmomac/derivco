# Derivco

## The solution

I tried to take the simplest and cleanest approach during the project.
The task needs 2 endpoints what can fetch from the same table, so the easies
solution was to start a phoenix project with single context.

I've got the data in a CSV, so I created a seed to import it.

Created 2 endpoint:

### [GET] /api/divseasons

This endpoint is fetching the unique Division/Season pairs from the database.

Example for json:
`http://localhost:4000/api/divseasons`

Example for Proto:
`http://localhost:4000/api/divseasons?format=proto`

### [GET] /api/results/<division>/<season>

This endpoint is fetching the all results from a Division/Season pair.

Example for json:
`http://localhost:4000/api/results/SP1/201617`

Example for Proto:
`http://localhost:4000/api/results/SP1/201617?format=proto`

### Rendering

By default both endpoints working with json,
but in case they get `format=proto` parameter
they'll reply with a Protobuf message.
(in case of any other value, they fallback on json)

# Local

To run locally:
* setup Postgres,
* Run `mix deps.get` to install dependencies.
* Run `mix ecto.setup` to create database and load seeds.
* Run `mix phx.server` to start the server.

# Docker

Since I have no experience in Docker with Elixir project on live environment,
I created only a simple solution to run he project:

To test it run `make up`
To populate database run `make initdb`
