# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Derivco.Repo.insert!(%Derivco.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Derivco.Sports.Result
alias Derivco.Repo

defmodule Derivco.Seeds do
  @moduledoc """
  Save entries to the database
  """

  @doc """
  Convert DD/MM/YYYY format to Date

  ## Examples

    iex> fix_date(%{date: "19/12/2019"})
    ~D[2019-12-19]
  """
  def fix_date(%{date: <<d1, d0, "/", m1, m0, "/", y3, y2, y1, y0>>} = row) do
    date = <<y3, y2, y1, y0, "-", m1, m0, "-", d1, d0>>
    date = Date.from_iso8601!(date)
    Map.update!(row, :date, fn _ -> date end)
  end

  @doc """
  Process a csv row and store it in DB
  """
  def store_it(row) do
    row = fix_date(row)
    changeset = Result.changeset(%Result{}, row)
    Repo.insert!(changeset)
  end
end

File.stream!("Data.csv")
|> Stream.drop(1)
|> CSV.decode(
  headers: [
    :id,
    :div,
    :season,
    :date,
    :hometeam,
    :awayteam,
    :fthg,
    :ftag,
    :frt,
    :hthg,
    :htag,
    :htr
  ]
)
|> Enum.each(&Derivco.Seeds.store_it/1)
