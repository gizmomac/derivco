defmodule Derivco.Repo.Migrations.CreateResults do
  use Ecto.Migration

  def change do
    create table(:results) do
      add :div, :string
      add :season, :string
      add :date, :date
      add :hometeam, :string
      add :awayteam, :string
      add :frt, :string
      add :htr, :string
      add :fthg, :integer
      add :ftag, :integer
      add :hthg, :integer
      add :htag, :integer
    end

  end
end
