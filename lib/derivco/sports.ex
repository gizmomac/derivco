defmodule Derivco.Sports do
  @moduledoc """
  The Sports context.
  """
  import Ecto.Query, warn: false
  alias Derivco.Repo
  alias Derivco.Sports.Result

  @doc ~S"""
  Fetch unique div and season pairs from the database
  """
  def list_divseasons() do
    from(r in Result,
      group_by: [r.season, r.div],
      select: [r.season, r.div],
      order_by: [r.div]
    )
    |> Repo.all()
  end

  @doc ~S"""
  Fetch results from the given div and season
  """
  def list_results(div, season) do
    from(r in Result, where: r.season == ^season and r.div == ^div)
    |> Repo.all()
  end
end
