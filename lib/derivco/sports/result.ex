defmodule Derivco.Sports.Result do
  @moduledoc """
  Database Schema module for results.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "results" do
    field :awayteam, :string
    field :date, :date
    field :div, :string
    field :frt, :string
    field :ftag, :integer
    field :fthg, :integer
    field :hometeam, :string
    field :htag, :integer
    field :hthg, :integer
    field :htr, :string
    field :season, :string
  end

  @doc false
  def changeset(result, attrs) do
    result
    |> cast(attrs, [
      :div,
      :season,
      :date,
      :hometeam,
      :awayteam,
      :frt,
      :htr,
      :fthg,
      :ftag,
      :hthg,
      :htag
    ])
    |> validate_required([
      :div,
      :season,
      :date,
      :hometeam,
      :awayteam,
      :frt,
      :htr,
      :fthg,
      :ftag,
      :hthg,
      :htag
    ])
  end
end
