defmodule Proto do
  @moduledoc """
  Protobuf Message definitions.
  """
  use Protobuf, """
  message Results {
      repeated Result results = 1;
  }

  message Result {
      int64 id = 1;
  	  string div = 2;
      string season = 3;
      string date = 4;
      string hometeam = 5;
      string awayteam = 6;
      int32 FTHG = 7;
      int32 FTAG = 8;
      string FTR = 9;
      int32 HTHG = 10;
      int32 HTAG = 11;
      string HTR = 12;
  }

  message Divseasons {
      repeated Divseason divseasons = 1;
  }

  message Divseason {
    string div = 1;
    string season = 2;
  }
  """
end
