defmodule DerivcoWeb.Router do
  use DerivcoWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", DerivcoWeb do
    pipe_through :api
    get("/divseasons/", ResultController, :divseasons) 
    get("/results/:div/:season", ResultController, :results)
  end
end
