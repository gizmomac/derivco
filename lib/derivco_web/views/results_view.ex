defmodule DerivcoWeb.ResultView do
  use DerivcoWeb, :view
  alias DerivcoWeb.ResultView

  @doc ~S"""
  Render divseasons to json.
  """
  def render("divseasons.json", %{results: results}) do
    render_many(results, ResultView, "divseason.json")
  end

  @doc ~S"""
  Render divseason to json.
  """
  def render("divseason.json", %{result: [div, season]}) do
    %{
      div: div,
      season: season
    }
  end

  @doc ~S"""
  Render results to json.
  """
  def render("results.json", %{results: results}) do
    render_many(results, ResultView, "result.json")
  end

  @doc ~S"""
  Render result to json.
  """
  def render("result.json", %{result: result}) do
    %{
      div: result.div,
      season: result.season,
      awayteam: result.awayteam,
      date: result.date,
      frt: result.frt,
      ftag: result.ftag,
      fthg: result.fthg,
      hometeam: result.hometeam,
      htag: result.htag,
      hthg: result.hthg,
      htr: result.htr
    }
  end

  @doc ~S"""
  Render divseasons to proto.
  """
  def render("divseasons.proto", %{results: results}) do
    %{
      divseasons:
        Enum.map(results, fn [x, y] ->
          Proto.Divseason.new(div: x, season: y)
        end)
    }
    |> Proto.Divseasons.new()
  end

  @doc ~S"""
  Render divseasons to proto.
  """
  def render("results.proto", %{results: results}) do
    %{
      results:
        Enum.map(results, fn result ->
          Proto.Result.new(%{
            div: result.div,
            season: result.season,
            awayteam: result.awayteam,
            date: Date.to_string(result.date),
            frt: result.frt,
            ftag: result.ftag,
            fthg: result.fthg,
            hometeam: result.hometeam,
            htag: result.htag,
            hthg: result.hthg,
            htr: result.htr
          })
        end)
    }
    |> Proto.Results.new()
  end
end
