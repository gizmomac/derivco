defmodule DerivcoWeb.ResultController do
  @moduledoc """
  Results Controller handling devseason and restults requests.
  """
  use DerivcoWeb, :controller

  alias Derivco.Sports

  @doc ~S"""
  Get div-season pairs from Sports and call the right
  render based on the 'type' GET parameter.
  """
  def divseasons(conn, params) do
    results = Sports.list_divseasons()

    case conn.params do
      %{"format" => "proto"} ->
        conn
        |> put_resp_content_type("application/x-protobuf")
        |> render("divseasons.proto", results: results)

      _ ->
        render(conn, "divseasons.json", results: results)
    end
  end

  @doc ~S"""
  Get results from the Sports module for the given div and season pair,
  and call the right render based on the 'type' GET parameter.
  """
  def results(conn, %{"div" => div, "season" => season} = _params) do
    results = Sports.list_results(div, season)

    case conn.params do
      %{"format" => "proto"} ->
        conn
        |> put_resp_content_type("application/x-protobuf")
        |> render("results.proto", results: results)

      _ ->
        render(conn, "results.json", results: results)
    end
  end
end
