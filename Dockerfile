FROM elixir:1.9.1
MAINTAINER Imre Guzmics <imre@guzmics.hu>

RUN mix local.hex --force \
 && mix archive.install --force hex phx_new 1.4.9 \
 && apt-get update \
 && apt-get install -y apt-utils \
 && mix local.rebar --force

ENV APP_HOME /app
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME
COPY ./ $APP_HOME/

EXPOSE 4000
RUN mix do deps.get

CMD ["mix", "phx.server"]
