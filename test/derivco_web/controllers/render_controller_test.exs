defmodule DerivcoWeb.ResultControllerTest do
  use DerivcoWeb.ConnCase

  alias Derivco.Sports.Result

  @valid_attrs_sp1_201617 %{
    div: "SP1",
    season: "201617",
    awayteam: "Eibar",
    hometeam: "La Coruna",
    date: ~D[2016-08-19],
    frt: "H",
    ftag: 1,
    fthg: 2,
    htag: 0,
    hthg: 0,
    htr: "D"
  }

  @valid_attrs_sp1_201516 %{
    div: "SP1",
    season: "201516",
    awayteam: "Eibar",
    hometeam: "La Coruna",
    date: ~D[2016-08-19],
    frt: "H",
    ftag: 1,
    fthg: 2,
    htag: 0,
    hthg: 0,
    htr: "D"
  }

  def result_fixture(attrs \\ %{}) do
    %Result{}
    |> Result.changeset(attrs)
    |> Repo.insert()
  end

  test "divseasons/2 responds with all unique divseasons", %{conn: conn} do
    result_fixture(@valid_attrs_sp1_201617)
    result_fixture(@valid_attrs_sp1_201617)
    result_fixture(@valid_attrs_sp1_201516)

    response =
      conn
      |> get(Routes.result_path(conn, :divseasons))
      |> json_response(200)

    expected = [
      %{"div" => "201516", "season" => "SP1"},
      %{"div" => "201617", "season" => "SP1"}
    ]

    assert response == expected
  end

  test "results/2 respons with all results from the divseason", %{conn: conn} do
    result_fixture(@valid_attrs_sp1_201617)
    result_fixture(@valid_attrs_sp1_201617)
    result_fixture(@valid_attrs_sp1_201516)

    response =
      conn
      |> get(Routes.result_path(conn, :results, "SP1", "201617"))
      |> json_response(200)

    expected = [
      %{
        "awayteam" => "Eibar",
        "date" => "2016-08-19",
        "div" => "SP1",
        "frt" => "H",
        "ftag" => 1,
        "fthg" => 2,
        "hometeam" => "La Coruna",
        "htag" => 0,
        "hthg" => 0,
        "htr" => "D",
        "season" => "201617"
      },
      %{
        "awayteam" => "Eibar",
        "date" => "2016-08-19",
        "div" => "SP1",
        "frt" => "H",
        "ftag" => 1,
        "fthg" => 2,
        "hometeam" => "La Coruna",
        "htag" => 0,
        "hthg" => 0,
        "htr" => "D",
        "season" => "201617"
      }
    ]

    assert response == expected
  end
end
