defmodule Derivco.SportsTest do
  use Derivco.DataCase

  alias Derivco.Sports

  describe "results" do
    alias Derivco.Sports.Result

    @valid_attrs_sp1_201617 %{
      div: "SP1",
      season: "201617",
      awayteam: "Eibar",
      hometeam: "La Coruna",
      date: ~D[2016-08-19],
      frt: "H",
      ftag: 1,
      fthg: 2,
      htag: 0,
      hthg: 0,
      htr: "D"
    }

    @valid_attrs_sp1_201516 %{
      div: "SP1",
      season: "201516",
      awayteam: "Eibar",
      hometeam: "La Coruna",
      date: ~D[2016-08-19],
      frt: "H",
      ftag: 1,
      fthg: 2,
      htag: 0,
      hthg: 0,
      htr: "D"
    }

    def result_fixture(attrs \\ %{}) do
      %Result{}
      |> Result.changeset(attrs)
      |> Repo.insert()
    end

    test "list_divseasons/0 returns unique div and season combination" do
      result_fixture(@valid_attrs_sp1_201617)
      result_fixture(@valid_attrs_sp1_201617)
      result_fixture(@valid_attrs_sp1_201516)

      assert Sports.list_divseasons() == [["201516", "SP1"], ["201617", "SP1"]]
    end

    test "list_results/2 returns unique div and season combination" do
      result_fixture(@valid_attrs_sp1_201617)
      result_fixture(@valid_attrs_sp1_201617)

      {:ok, sp1_201516} = result_fixture(@valid_attrs_sp1_201516)
      assert Sports.list_results("SP1", "201516") == [sp1_201516]
    end
  end
end
